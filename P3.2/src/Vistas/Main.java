package Vistas;

import Controladores.ClienteController;
import Controladores.Cliente_EmpleadoController;
import Controladores.EmpleadoController;
import Modelos.Cliente;
import Modelos.Cliente_Empleado;
import Modelos.Empleado;
import Script.Conexion;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class Main{
    
public static void main (String[] args) throws IOException, SQLException{
    boolean salir=false;
         Conexion cone = new Conexion();
         Connection con = cone.getConnection();
    try{
        
     Scanner sc = new Scanner(System.in);

     while (salir==false){
         
       // System.err.println("Inserta el numero de la tabla que quieres usar, de lo contrario, dará error");
        System.out.println("\nSelecciona una tabla:");
        System.out.println("\n1. Cliente");
        System.out.println("\n2. Empleado");
        System.out.println("\n3. Cliente-Empleado");
        System.out.println("\n4. Salir");
        String ele = sc.next();
        String vuelta = "";
        switch(ele){
            //Cliente
            case "1":{
                ClienteController cl = new ClienteController(con);
                boolean menu = true;
                while(menu){
                vuelta = mostrar("cliente");
                switch (vuelta){
                    //Listar
                    case "1":{
                        System.out.println(cl.listarClientes());
                        break;
                    }
                    //Listar desde cuanto
                    case "2":{
                        int d,c;
                        try{
                        Scanner t = new Scanner(System.in);
                        System.out.println("\nDesde que ID desea motrar?");
                        d=t.nextInt();
                        System.out.println("\nCuantos filas desea mostrar?");
                        c=t.nextInt();
                            System.out.println(cl.listarClientes(d,c));
                           
                        }catch(Exception e){
                            System.err.println("El valor a ingresar debe ser un número");
                        }
                        break;
                    }
                    //Insert
                    case "3":{
                        String nombre,apellido,direccion,telefono;
                        try{
                        Scanner t = new Scanner(System.in);
                        System.out.println("\nIngresar Nombre de Cliente");
                        nombre=t.nextLine();
                        System.out.println("\nIngresar Apellido de Cliente");
                        apellido=t.nextLine();
                        System.out.println("\nIngresar Direccion del Cliente");
                        direccion=t.nextLine();
                        System.out.println("\nIngresar telefono del Cliente");
                        telefono=t.next();
                        cl.insertCliente(nombre, apellido, direccion, telefono);
                        }catch (Exception e){
                            System.err.println("Error en el ingreso de datos");
                        }
                        break;
                    }
                    //Update
                    case "4":{
                        String nombre,apellido,direccion,telefono;
                        String id;
                        try{
                        Scanner s= new Scanner(System.in);
                        System.out.println("\nIngresar ID del Cliente");
                        id= s.nextLine();
                        System.out.println("\nIngresar Nombre de Cliente");
                        nombre=s.nextLine();
                        System.out.println("\nIngresar Apellido de Cliente");
                        apellido=s.nextLine();
                        System.out.println("\nIngresar Direccion del Cliente");
                        direccion=s.nextLine();
                        System.out.println("\nIngresar telefono del Cliente");
                        telefono=s.nextLine();
                        System.out.println(cl.updateCliente(new Cliente(Integer.parseInt(id),nombre, apellido, direccion, telefono)));
                       
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                        break;
                    }
                    //Delete
                    case "5":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID del Cliente");
                        int i= s.nextInt();
                        cl.deleteCliente(i);
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                    break;
                    }
                    //Get
                    case "6":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID del Cliente");
                        int i= s.nextInt();
                        System.out.println(cl.getCliente(i));
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                        break;
                    }
                    //GetLike
                    case "7":{
                        String aux;
                         Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Nombre del Cliente");
                        aux= s.nextLine();
                        System.out.println(cl.getClienteLike(aux));
                        break;
                    }
                    //GetWhere
                    case "8":{
                        String aux;
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Dirección del Cliente");
                        aux= s.nextLine();
                        System.out.println(cl.getClienteWhere(aux));
                        break;
                    }
                    //Atras
                    case "9":{
                        menu=false;
                        break;
                    }   
                    default:{
                        System.err.println("Ingresar Alguna opción de las indicadas anteriormente");
                    }
                }
                    
        }
                break;
            }
            //Empleado
            case "2":{
                EmpleadoController em = new EmpleadoController(con);
                boolean menu = true;
                while(menu){
                vuelta = mostrar("empleado");
                switch (vuelta){
                    //Listar
                    case "1":{
                        System.out.println(em.listarEmpleados());
                        break;
                    }
                    //Listar desde cuanto
                    case "2":{
                        int d,c;
                        try{
                        Scanner t = new Scanner(System.in);
                        System.out.println("\nDesde que ID desea motrar?");
                        d=t.nextInt();
                        System.out.println("\nCuantos filas desea mostrar?");
                        c=t.nextInt();
                            System.out.println(em.listarEmpleado(d,c));
                           
                        }catch(Exception e){
                            System.err.println("\nEl valor a ingresar debe ser un número");
                        }
                        break;
                    }
                    //Insert
                    case "3":{
                        String nombre,apellido,direccion,telefono;
                        try{
                        Scanner t = new Scanner(System.in);
                        System.out.println("\nIngresar Nombre de Empleado");
                        nombre=t.nextLine();
                        System.out.println("\nIngresar Apellido de Empleado");
                        apellido=t.nextLine();
                        System.out.println("\nIngresar Direccion del Empleado");
                        direccion=t.nextLine();
                        System.out.println("\nIngresar telefono del Empleado");
                        telefono=t.next();
                        em.insertEmpleado(nombre, apellido, direccion, telefono);
                        }catch (Exception e){
                            System.err.println("Error en el ingreso de datos");
                        }
                        break;
                    }
                    //Update
                    case "4":{
                        String nombre,apellido,direccion,telefono;
                        String id;
                        try{
                        Scanner s= new Scanner(System.in);
                        System.out.println("\nIngresar ID del Empleado");
                        id= s.nextLine();
                        System.out.println("\nIngresar Nombre de Empleado");
                        nombre=s.nextLine();
                        System.out.println("\nIngresar Apellido de Empleado");
                        apellido=s.nextLine();
                        System.out.println("\nIngresar Direccion del Empleado");
                        direccion=s.nextLine();
                        System.out.println("\nIngresar telefono del Empleado");
                        telefono=s.nextLine();
                            System.out.println(em.updateEmpleado(new Empleado(Integer.parseInt(id),nombre,apellido,direccion,telefono)));
                       
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                        break;
                    }
                    //Delete
                    case "5":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID del Empleado");
                        int i= s.nextInt();
                        em.deleteEmpleado(i);
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                    break;
                    }
                    //Get
                    case "6":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID del Empleado");
                        int i= s.nextInt();
                        System.out.println(em.getEmpleado(i));
                        }catch (Exception e){
                            System.err.println("ID debe ser un numero");
                        }
                        break;
                    }
                    //GetLike
                    case "7":{
                        String aux;
                         Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Nombre del Empleado");
                        aux= s.nextLine();
                        System.out.println(em.getEmpleadoLike(aux));
                        break;
                    }
                    //GetWhere
                    case "8":{
                        String aux;
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Dirección del Empleado");
                        aux= s.nextLine();
                        System.out.println(em.getEmpleadoWhere(aux));
                        break;
                    }
                    //Atras
                    case "9":{
                        menu=false;
                        break;
                    }   
                    default:{
                        System.err.println("Ingresar Alguna opción de las indicadas anteriormente");
                    }
                }
                    
        }
                break;                
            }
            //Cliente_Empleado
            case "3":{
                Cliente_EmpleadoController em = new Cliente_EmpleadoController(con);
                boolean menu = true;
                while(menu){
                vuelta = mostrar("cli-emp");
                switch (vuelta){
                    //Listar
                    case "1":{
                        System.out.println(em.listarCliente_Empleado());
                        break;
                    }
                    //Listar desde cuanto
                    case "2":{
                        int d,c;
                        try{
                        Scanner t = new Scanner(System.in);
                        System.out.println("\nDesde que Codigo desea motrar?");
                        d=t.nextInt();
                        System.out.println("\nCuantas filas desea mostrar?");
                        c=t.nextInt();
                            System.out.println(em.listarCliente_Empleado(d,c));
                           
                        }catch(Exception e){
                            System.err.println("El valor a ingresar debe ser un número");
                        }
                        break;
                    }
                    //Insert
                    case "3":{
                        int ide,idc;
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nID de Empleado");
                        ide=s.nextInt();
                        System.out.println("\nID de Cliente");
                        idc=s.nextInt();
                        em.insertCliente_Empleado(idc,ide);
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                        break;
                    }
                    //Update
                    case "4":{
                        int cod,ide,idc;
                        try{
                        Scanner s= new Scanner(System.in);
                        System.out.println("\nCodigo:");
                        cod=s.nextInt();                        
                        System.out.println("\nID de Empleado:");
                        ide=s.nextInt();
                        System.out.println("\nID de Cliente:");
                        idc=s.nextInt();
                            System.out.println(em.updateCliente_Empleado(new Cliente_Empleado(cod,ide,idc)));
                       
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                        break;
                    }
                    //Delete
                    case "5":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Codigo del Cliente_Empleado");
                        int i= s.nextInt();
                        em.deleteCliente_Empleado(i);
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                    break;
                    }
                    //Get
                    case "6":{
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar Codigo del Cliente_Empleado");
                        int i= s.nextInt();
                        System.out.println(em.getCliente_Empleado(i));
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                        break;
                    }
                    //GetCliente
                    case "7":{
                        int aux;
                        try{
                         Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID de Cliente");
                        aux= s.nextInt();
                        System.out.println(em.getCliente(aux));
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                        break;
                    }
                    //GetEmpleado
                    case "8":{
                        int aux;
                        try{
                        Scanner s = new Scanner(System.in);
                        System.out.println("\nIngresar ID de Empleado");
                        aux= s.nextInt();
                        System.out.println(em.getEmpleado(aux));
                        }catch (Exception e){
                            System.err.println("Debe ingresar solo numeros");
                        }
                        break;
                    }
                    //Atras
                    case "9":{
                        menu=false;
                        break;
                    }   
                    default:{
                        System.err.println("Ingresar Alguna opción de las indicadas anteriormente");
                    }
                }
                    
        }
                break;                
            }
            //Salir
            case "4":{
                salir=true;
                break;
            }
            default:
                System.err.println("Ingresar Alguna opción de las indicadas anteriormente");
            
            
        }
    }
    }catch (Exception e){
        System.err.println(e);
    }
con.close();

}
public static String mostrar(String tabla){ //Muestra el Menu especifico para cada tabla
    String vuel="";
    if (!tabla.equals("cliente")&&!tabla.equals("empleado")&&!tabla.equals("cli-emp")){  //verifica si la tabla ingresada es correcta
        System.err.println("Error: La tabla no es correcta");
    }else{
      //  System.err.println("Inserta el numero de la opción que quieres usar, de lo contrario, dará error");
        Scanner scc = new Scanner(System.in);
        
        System.out.println("\n1. Listar <"+tabla+">");
        System.out.println("\n2. Listar <"+tabla+"> <desde> <cuanto>.  Listará el contenido de la tabla de forma paginada.");
        System.out.println("\n3. Insertar <"+tabla+">. Desplegará las peticiones necesarias para generar una filanueva en la tabla seleccionada.");
        System.out.println("\n4. Actualizar <"+tabla+"> <id>. Desplegará las peticiones necesarias para actualizaruna fila existente en la tabla seleccionada para el id seleccionado.");
        System.out.println("\n5. Borrar <"+tabla+"> <id>. Borrará la fila de tabla que posea ese id.");
        System.out.println("\n6. Obtener <"+tabla+"> <id>. Se mostrará la fila de la tabla que coincida con el id ingresado");
                 if (tabla.equals("cliente")||tabla.equals("empleado")){ //Las opciones extra de Clientes y Empleados son las mismas
        System.out.println("\n7. Obtener <"+tabla+"> <nombre>. Se mostrarán las filas de la tabla que coincidan con el nombre ingresado");
        System.out.println("\n8. Obtener <"+tabla+"> <direccion>. Se mostrarán las filas de la tabla que coincidan con la direccion ingresada");
                 }else{ //Opciones extra de Cliente-Empleado
         System.out.println("\n7. Listar <"+tabla+"> <id_cliente>. Listará el contenido de la tabla que coincida con el id_cliente ingresado.");      
         System.out.println("\n8. Listar <"+tabla+"> <id_empleado>. Listará el contenido de la tabla que coincida con el id_empleado ingresado.");    
             }
         System.out.println("\n9. Atras");
        vuel=scc.next();
        do{
          if (!vuel.equals("1")&&!vuel.equals("2")&&!vuel.equals("3")&&!vuel.equals("4")&&!vuel.equals("5")&&!vuel.equals("6")&&!vuel.equals("7")&&!vuel.equals("8")&&!vuel.equals("9")){
                System.err.println("Error: Ha ingresado un valor erroneo. Por favor, ingrese el numero de la opcion que desea realizar");
                vuel=scc.next();
          }
    }while (!vuel.equals("1")&&!vuel.equals("2")&&!vuel.equals("3")&&!vuel.equals("4")&&!vuel.equals("5")&&!vuel.equals("6")&&!vuel.equals("7")&&!vuel.equals("8")&&!vuel.equals("9"));
    

        
}
    return vuel;
}
}