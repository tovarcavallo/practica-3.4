/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;




/**
 *
 * @author carlo
 */
public class Cliente_Empleado {
    int codigo;
    int idEmpleado;
    int idCliente;
   

    public Cliente_Empleado(int codigo,int idEmpleado, int idCliente) {
        this.codigo=codigo;
        this.idEmpleado = idEmpleado;
        this.idCliente = idCliente;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public String toString() {
        return "\nCliente_Empleado "+ codigo + ":\nIdEmpleado=" + idEmpleado + "\nIdCliente=" + idCliente +"\n";
    }


    
}
