/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.List;

/**
 *
 * @author carlo
 */
public interface EmpleadoDAO {
    public List<Empleado> listarEmpleados();
    public List<Empleado> listarEmpleado(int desde, int cuanto);
    public boolean insertEmpleado(String nombre, String apellido, String direccion, String telefono);
    public Empleado updateEmpleado(Empleado empleado);
    public boolean deleteEmpleado(int idEmpleado);
    public Empleado getEmpleado(int idEmpleado);
    public List<Empleado> getEmpleadoLike(String nombre);
    public List<Empleado> getEmpleadoWhere(String direccion);
    
}
