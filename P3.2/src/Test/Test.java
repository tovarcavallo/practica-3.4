
package Test;

import Script.Conexion;
import Modelos.Cliente;
import Controladores.ClienteController;
import Controladores.Cliente_EmpleadoController;
import Controladores.EmpleadoController;
import Modelos.Cliente_Empleado;
import Modelos.Empleado;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class Test {

    public void test() throws IOException, SQLException {
          Conexion cone = new Conexion();
     Connection con = cone.getConnection();
     ClienteController cl = new ClienteController(con);
        System.out.println("Clientes:_______________________________________________________________________________________________");
        System.out.println("Listar: "+cl.listarClientes(0, 3));
        System.out.println("Insert: "+cl.insertCliente("Paloma", "Perez", "Plaza eliptica", "102938475"));
        System.out.println("Update: "+cl.updateCliente(new Cliente(6,"Prof. Paloma","Perez","Calle Polo Norte","000000000")));
        System.out.println("Get id 6: "+cl.getCliente(6));
        System.out.println("Delete id 6: "+cl.deleteCliente(6));
        System.out.println("GetLike Señora: "+cl.getClienteLike("Señora"));
        System.out.println("GetWhere Calle Licencia: "+cl.getClienteWhere("Calle Licencia"));
     EmpleadoController em = new EmpleadoController(con);
                System.out.println("Empleados:_______________________________________________________________________________________________");
        System.out.println("Listar: "+em.listarEmpleado(0, 2));
        System.out.println("Insert: "+em.insertEmpleado("Alejandro", "Sifontes", "Plaza eliptica", "102938475"));
        System.out.println("Update: "+em.updateEmpleado(new Empleado(6,"Lolita","Sifontes","Calle Polo Norte","000000000")));
        System.out.println("Get id 6: "+em.getEmpleado(6));
        System.out.println("Delete id 6: "+em.deleteEmpleado(6));
        System.out.println("GetLike Carlos: "+em.getEmpleadoLike("Carlos"));
        System.out.println("GetWhere Olivos: "+em.getEmpleadoWhere("Olivos"));
     Cliente_EmpleadoController ce = new Cliente_EmpleadoController(con);
                System.out.println("Cliente_Empleado:_______________________________________________________________________________________________");
        System.out.println("Listar: "+ce.listarCliente_Empleado(0, 5));     
        System.out.println("Insert: "+ce.insertCliente_Empleado(2, 5));
        System.out.println("Update: "+ce.updateCliente_Empleado(new Cliente_Empleado(2,3,4)));
       //   System.out.println("Delete codigo 4: "+ce.deleteCliente_Empleado(4));
        System.out.println("Get codigo 2: "+ce.getCliente_Empleado(2));
        System.out.println("GetCliente id 2: "+ce.getCliente(2));
        System.out.println("GetEmpleado id 3: "+ce.getEmpleado(3));
        
        
        
     
        
        

     
     
     
     
        Conexion.closeConnection(con);
    }
    
}
