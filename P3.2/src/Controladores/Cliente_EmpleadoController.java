/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Modelos.Cliente;
import Modelos.Cliente_Empleado;
import Modelos.Cliente_EmpleadoDAO;
import Modelos.Empleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class Cliente_EmpleadoController implements Cliente_EmpleadoDAO {
    
Connection con;
private static final String SELECT_ALL_CLIENTE_EMPLEADO = "Select * from cliente_empleado order by codigo";
private static final String SELECT_ALL_CLIENTE_EMPLEADO_PAGINATION = "Select * from cliente_empleado order by codigo limit ? offset ?";
 private static final String INSERT_CLIENTE_EMPLEADO_QUERY="insert into cliente_empleado (id_empleado,id_cliente) values";
  private static final String UPDATE_CLIENTE_EMPLEADO = "update cliente_empleado SET id_empleado = ?, id_cliente = ?  where codigo = ?";
  private static final String DELETE_CLIENTE_EMPLEADO = "delete from cliente_empleado where codigo = ?";
   private static final String SELECT_CLIENTE_EMPLEADO = "select * from cliente_empleado where codigo = ?" ; 
   private static final String SELECT_CLIENTE_LIKE = "select * from cliente_empleado where id_cliente = ?";
   private static final String SELECT_EMPLEADO_LIKE = "select * from cliente_empleado where id_empleado = ?";
  

public Cliente_EmpleadoController (Connection con){
    super();
    this.con=con;
}
    @Override
    public List<Cliente_Empleado> listarCliente_Empleado() { // devuelve lista de cliente_empleados, el usuario decide desde que codigo y cuentos mostrar
                     PreparedStatement st; 
        List<Cliente_Empleado> climple = new ArrayList<>();
        try{
            st = con.prepareStatement(SELECT_ALL_CLIENTE_EMPLEADO);

            ResultSet rs=  st.executeQuery();
            
            while(rs.next()){
                int cod = rs.getInt("codigo");
                int idE = rs.getInt("id_empleado");
                int idC = rs.getInt("id_cliente");

                
                Cliente_Empleado cmp = new Cliente_Empleado(cod,idE,idC);
                climple.add(cmp);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         climple = null;
     }
        return climple;
    }
    
    @Override
    public List<Cliente_Empleado> listarCliente_Empleado(int desde, int cuanto) { // devuelve lista de cliente_empleados, el usuario decide desde que codigo y cuentos mostrar
                     PreparedStatement st; 
        List<Cliente_Empleado> climple = new ArrayList<>();
        try{
            st = con.prepareStatement(SELECT_ALL_CLIENTE_EMPLEADO_PAGINATION);
            st.setInt(1, cuanto);
            st.setInt(2, desde);
            ResultSet rs=  st.executeQuery();
            
            while(rs.next()){
                int cod = rs.getInt("codigo");
                int idE = rs.getInt("id_empleado");
                int idC = rs.getInt("id_cliente");

                
                Cliente_Empleado cmp = new Cliente_Empleado(cod,idE,idC);
                climple.add(cmp);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         climple = null;
     }
        return climple;
    }

    @Override
    public boolean insertCliente_Empleado(int idCliente, int idEmpleado) { //Para insertar cliente_empleado al Restaurante, devuelve un boolean que valdrá true si lo logra y false si no pudo lograrlo
                        boolean u= true;
        Statement st=null;
        int num;
        try{
          st=con.createStatement();
          num = st.executeUpdate(INSERT_CLIENTE_EMPLEADO_QUERY + " ("+"\""+idEmpleado+"\""+","+"\""+idCliente+"\""+")"); 
          
          if (num==1){
              u = true;
              System.err.println("El Cliente_Empleado ha sido ingresado de forma exitosa");
          }else{
              System.err.println("Error: No se ha podido insertar Cliente_Empleado");
              u = false;
          }
          } catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
          }
        finally{
            try{
                if (st == null)
                    st.close();
            }catch (SQLException sqle) {
                sqle.printStackTrace();  
            }
        }
            
        return u;
    }

    @Override
    public Cliente_Empleado updateCliente_Empleado(Cliente_Empleado cliemple) {  //Para cambiar datos de cliente_empleado, devuelve cliente_empleado acualizado
                     Cliente_Empleado clemp = null;
       int cod = cliemple.getCodigo();
       clemp = getCliente_Empleado(cod);
       if (clemp!=null){
           if (cliemple.getIdEmpleado()==clemp.getIdEmpleado() && cliemple.getIdCliente()==clemp.getIdCliente() && cliemple.getCodigo()==clemp.getCodigo()){
               System.out.println("No se han observado cambios");
           }else{
               PreparedStatement ps;
               try{
                   ps = con.prepareStatement(UPDATE_CLIENTE_EMPLEADO);
                   ps.setInt(1, cliemple.getIdEmpleado());
                   ps.setInt(2, cliemple.getIdCliente());
                   ps.setInt(3, cliemple.getCodigo());
                   
                   int aux = ps.executeUpdate();
                   
                   if (aux==1){
                       clemp= cliemple;
                   }else{
                       clemp=null;
                   }
               } catch (SQLException ex) {
                   ex.printStackTrace();
               }
           }
       }else
            System.err.println("Error: No existe un Cliente_Empleado con ese codigo");
       
       return clemp;
    }

    @Override
    public boolean deleteCliente_Empleado(int codigo) { //Para eliminar cliente_empleado, devulve boolean que valdrá true si lo logra, y false si no lo logra
                     Cliente_Empleado clemp = getCliente_Empleado(codigo);
         boolean u = false;
         
         if (clemp!=null){
             PreparedStatement st = null;
             
             try{
                 st = con.prepareStatement(DELETE_CLIENTE_EMPLEADO);
                 st.setInt(1, codigo);
                 int num = st.executeUpdate();
                   if (num == 1){
                       u = true;
                       System.err.println("El Cliente_Empleado ha sido eliminado de forma exitosa");
                   }else{
                       u=false;
                   }
             } catch (SQLException ex) {
                 ex.printStackTrace();
             }
             }else{
             System.err.println("Error: No existe un cliente_empleado con el codigo ingresado");
         }
         return u;
    }

    @Override
    public Cliente_Empleado getCliente_Empleado(int codigo) { //Devuelve cliente con codigo ingresado por el usuario
                 PreparedStatement st;
        Cliente_Empleado clemp = null;
        try{
            st = con.prepareStatement(SELECT_CLIENTE_EMPLEADO);
            st.setInt(1, codigo);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()){
                int cod = rs.getInt("codigo");
                int idE = rs.getInt("id_empleado");
                int idC = rs.getInt("id_cliente");
                
                clemp = new Cliente_Empleado (cod,idE,idC);
            }else{
                System.err.println("Error: No existe un cliente_empleado con el codigo ingresado");
                
            }
        } catch (SQLException ex) {
        ex.printStackTrace();
     }
        return clemp;
    }


    @Override
    public List<Cliente_Empleado> getCliente(int idCliente) { //Lista de cliente_empleado donde el id_cliente sea el ingresado por el usuario
          PreparedStatement st; 
          List<Cliente_Empleado> cliemp = new ArrayList<>();
          try{
              st = con.prepareStatement(SELECT_CLIENTE_LIKE);
              st.setInt(1, idCliente);
              ResultSet rs = st.executeQuery();
              
           while(rs.next()){
                int cod = rs.getInt("codigo");
                int idE = rs.getInt("id_empleado");
                int idC = rs.getInt("id_cliente");

                
                Cliente_Empleado cmp = new Cliente_Empleado(cod,idE,idC);
                cliemp.add(cmp);
            }
        } catch (SQLException ex) {
              System.err.println(ex);
         cliemp = null;
     }
        return cliemp;
    }
       
        
        

    @Override
    public List<Cliente_Empleado> getEmpleado(int idEmpleado) { //Lista de cliente_empleado donde el id_empleado sea el ingresado por el usuario
                  PreparedStatement st; 
          List<Cliente_Empleado> cliemp = new ArrayList<>();
          try{
              st = con.prepareStatement(SELECT_EMPLEADO_LIKE);
              st.setInt(1, idEmpleado);
              ResultSet rs = st.executeQuery();
              
           while(rs.next()){
                int cod = rs.getInt("codigo");
                int idE = rs.getInt("id_empleado");
                int idC = rs.getInt("id_cliente");

                
                Cliente_Empleado cmp = new Cliente_Empleado(cod,idE,idC);
                cliemp.add(cmp);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         cliemp = null;
     }
        return cliemp;
    }
    

    
}
