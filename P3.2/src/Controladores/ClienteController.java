/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;


import Script.Conexion;
import Modelos.Cliente;
import Modelos.ClienteDAO;
import java.io.IOException;

import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author carlo
 */
public class ClienteController implements ClienteDAO {
    Connection con;
 private static final String SELECT_ALL_CLIENTES= "Select * from cliente order by id";
 private static final String SELECT_ALL_CLIENTES_PAGINATION = "Select * from cliente order by id limit ? offset ?";
 private static final String INSERT_CLIENTE_QUERY="insert into cliente (nombre,apellido,direccion,telefono) values";
 private static final String DELETE_CLIENTE = "delete from cliente where id = ?";
 private static final String SELECT_CLIENTE = "select * from cliente where id = ?" ;         
 private static final String UPDATE_CLIENTE = "update cliente SET nombre = ?, apellido = ?, direccion = ?, telefono = ?  where id = ?";
 private static final String SELECT_CLIENTE_LIKE = "select * from cliente where nombre = ";
 private static final String SELECT_CLIENTE_WHERE = "select * from cliente where direccion = ";
 public ClienteController(Connection con) {
     super();
     this.con=con;
 }
 
     @Override
    public List<Cliente> listarClientes() {  // devuelve lista de clientes
        PreparedStatement st; 
        List<Cliente> Clientes = new ArrayList<>();
        try{
            st = con.prepareStatement(SELECT_ALL_CLIENTES);
            ResultSet rs=  st.executeQuery();
            
            while(rs.next()){
                int idC = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                Cliente cli = new Cliente(idC,nombreC,apellidoC,direccionC,telefonoC);
                Clientes.add(cli);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         Clientes = null;
     }
        return Clientes;
    }

    @Override
    public List<Cliente> listarClientes(int desde, int cuanto) {  // devuelve lista de clientes
        PreparedStatement st; 
        List<Cliente> Clientes = new ArrayList<>();
        try{
            st = con.prepareStatement(SELECT_ALL_CLIENTES_PAGINATION);
            st.setInt(1, cuanto);
            st.setInt(2, desde);
            ResultSet rs=  st.executeQuery();
            
            while(rs.next()){
                int idC = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                Cliente cli = new Cliente(idC,nombreC,apellidoC,direccionC,telefonoC);
                Clientes.add(cli);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         Clientes = null;
     }
        return Clientes;
    }

    @Override
     public boolean insertCliente(String nombre, String apellido, String direccion, String telefono) throws SQLException { //Para insertar clientes al Restaurante, devuelve un boolean que valdrá true si lo logra y false si no pudo lograrlo
        boolean u = false;
        Statement st=null;
        int num;
        try{
        st=con.createStatement();
            //Inserto
          num = st.executeUpdate(INSERT_CLIENTE_QUERY + " ("+"\""+nombre+"\""+","+"\""+apellido+"\""+","+"\""+direccion+"\""+","+"\""+telefono+"\""+")");
          
          if (num==1){
              System.err.println("El Cliente se ha ingresado de forma exitosa");
              u=true;
              
          }else{
              System.out.println("Error: No se ha podido insertar Cliente");
              u = false;
          }
          } catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
          }
        finally{
            try{
                if (st == null)
                    st.close();
             
                
            }catch (SQLException sqle) {
                sqle.printStackTrace();  
            }
        
            
        
        }
         return u;
    }

    @Override
    public Cliente updateCliente(Cliente cliente) { //Para cambiar datos de cliente, devuelve cliente acualizado
       Cliente cli = null;
       int id = cliente.getId();
       cli = getCliente(id);
       if (cli!=null){
           if (cliente.getNombre().equals(cli.getNombre()) && cliente.getApellido().equals(cli.getApellido()) && cliente.getDireccion().equals(cli.getDireccion()) && cliente.getTelefono().equals(cli.getTelefono())){
               System.out.println("No se han observado cambios");
           }else{
               PreparedStatement ps;
               try{
                   ps = con.prepareStatement(UPDATE_CLIENTE);
                   ps.setString(1, cliente.getNombre());
                   ps.setString(2, cliente.getApellido());
                   ps.setString(3, cliente.getDireccion());
                   ps.setString(4, cliente.getTelefono());
                   ps.setInt(5, cliente.getId());
                   
                   int aux = ps.executeUpdate();
                   
                   if (aux==1){
                       cli= cliente;
                   }else{
                       cli=null;
                   }
               } catch (SQLException ex) {
                   ex.printStackTrace();
               }
           }
       }else
            System.err.println("Error: No existe un cliente con ese id");
       return cli;
    }

    @Override
    public boolean deleteCliente(int idCliente) {  //Para eliminar cliente, devulve boolean que valdrá true si lo logra, y false si no lo logra
         Cliente cli = getCliente(idCliente);
         boolean u = false;
         
         if (cli!=null){
             PreparedStatement st = null;
             
             try{
                 st = con.prepareStatement(DELETE_CLIENTE);
                 st.setInt(1, idCliente);
                 int num = st.executeUpdate();
                   if (num == 1){
                       System.err.println("El cliente ha sido Eliminado de manera exitosa");
                       u = true;
                   }else{
                       u=false;
                   }
             } catch (SQLException ex) {
                 ex.printStackTrace();
             }
             }else{
             System.err.println("Error: No existe un cliente con el id ingresado");
         }
         return u;
    }

    @Override
    public Cliente getCliente(int idCliente) { //Devuelve cliente con id ingresado por el usuario
        PreparedStatement st;
        Cliente cli = null;
        try{
            st = con.prepareStatement(SELECT_CLIENTE);
            st.setInt(1, idCliente);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()){
                int idC = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                cli = new Cliente (idC,nombreC,apellidoC,direccionC,telefonoC);
            }else{
                System.err.println("Error: No existe un cliente con el id ingresado");
                
            }
        } catch (SQLException ex) {
        ex.printStackTrace();
     }
        return cli;
    }

    @Override
    public List<Cliente> getClienteLike(String nombre) { //Devuelve una lista con todos los clientes con el nombre que el usuario indique
        PreparedStatement st; 
        List<Cliente> clientes = null;
          try{
              clientes= new ArrayList<>();
              st = con.prepareStatement(SELECT_CLIENTE_LIKE +"\""+ nombre+"\"");
              ResultSet rs=  st.executeQuery();
              
              while (rs.next()){
                int idC = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                Cliente cli = new Cliente(idC,nombreC,apellidoC,direccionC,telefonoC);
                clientes.add(cli);
              }
              if (clientes.isEmpty()){  //Si no hay nada en la lista, es porque no hay ninguna coincidencia
                  System.err.println("Error: El nombre ingresado no se encuentra como Cliente");
                  
              }
              
          } catch (SQLException ex) {
        ex.printStackTrace();
        clientes = null;
          }

            return clientes;
     }
    

    @Override
    public List<Cliente> getClienteWhere(String direccion) {  //Devuelve una lista con todos los clientes con la direccion que el usuario indique
                PreparedStatement st; 
        List<Cliente> clientes = null;
          try{
              clientes= new ArrayList<>();
              st = con.prepareStatement(SELECT_CLIENTE_WHERE + "\""+direccion+"\"");
              ResultSet rs=  st.executeQuery();
              
              while (rs.next()){
                int idC = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                Cliente cli = new Cliente(idC,nombreC,apellidoC,direccionC,telefonoC);
                clientes.add(cli);
              }
                if (clientes.isEmpty()){  //Si no hay nada en la lista, es porque no hay ninguna coincidencia
                  System.err.println("Error: La direccion ingresada no se encuentra como Cliente");
                  
              }
              
          } catch (SQLException ex) {
        ex.printStackTrace();
        clientes = null;
          }

            return clientes;
    }


    
}
