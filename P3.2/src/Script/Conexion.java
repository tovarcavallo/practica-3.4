package Script;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;



public class Conexion {

	public String dbms;
	public String dbName;
	public String userName;
	public String password;
	public String urlString;

	public String driver;
	public String serverName;
	public int portNumber;
	

	public Conexion()throws FileNotFoundException, IOException,InvalidPropertiesFormatException {
		
		setProperties();
	}

	/**
	 * Asignación de propiedades de conexión de xml a atributos de clase
	 * @param fileName
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws InvalidPropertiesFormatException
	 */
	private void setProperties() throws IOException, InvalidPropertiesFormatException {
                
		this.dbms = "mysql";                          
		this.driver = "com.mysql.jdbc.Driver";            
		this.dbName = "restaurante";
		this.userName = "root";
		this.password = "TovarCity";
                this.urlString= "jdbc:mysql://localhost:3306/restaurante?serverTimezone=UTC";
		this.serverName = "localhost";              
		this.portNumber = 3306;


	}

	/**
	 * Conexion a Base de Datos
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {

         Connection conn = DriverManager.getConnection(this.urlString, this.userName, this.password);
            System.err.println("Conexion realizada con Exito");
         return conn;
	}

	/**
	 * Cierre de conexión a BD
	 * @param connArg
	 */
	public static void closeConnection(Connection conn) throws SQLException {
         conn.close();
	}
	
	
}
